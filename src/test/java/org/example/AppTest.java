package org.example;

import org.example.config.SpringConfig;
import org.example.model.City;
import org.example.model.Logistics;
import org.example.model.Transport;
import org.example.model.Truck;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.Assert.*;


public class AppTest 
{
    City moscow = new City("Moscow", 300);
    ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
    Logistics logistics = context.getBean(Logistics.class);
    Transport result= logistics.getShipping(moscow, 567, 7);
    @Test
    public void shouldCreateTransport(){
        assertNotNull(logistics.getVehicles());
        assertTrue(logistics.getVehicles().length > 0);
    }
    @Test
    public void shouldCreateDefaultTruck(){
        assertEquals(Truck.class, result.getClass());
        assertEquals("Truck", logistics.getVehicles()[0].getName());
    }
}
