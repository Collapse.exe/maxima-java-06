package org.example.config;

import org.example.model.TransportFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = "org.example")
public class SpringConfig {
    @Bean("factory")
    public TransportFactory transportFactory(){
        return new TransportFactory();
    }
}
