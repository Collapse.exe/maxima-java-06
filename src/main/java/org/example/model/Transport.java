package org.example.model;

abstract public class Transport implements Repairable {
    private String name;
    private int capacity;
    private int speed;
    private float costOfKm;
    private boolean repairingStatus;

    @Override
    public void startRepair() {
        repairingStatus = true;
    }

    @Override
    public void finishRepair() {
        repairingStatus = false;
    }

    @Override
    public boolean isRepairing() {
        return repairingStatus;
    }

    abstract public float getPrice (City city);

    public float getShippingTime(City city){
        return (float)city.getDistanceKm() / (float)this.speed;
    }

    public Transport(String name, int capacity, int speed, float costOfKm) {
        this.name = name;
        this.capacity = capacity;
        this.speed = speed;
        this.costOfKm = costOfKm;
    }

    protected Transport() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public float getCostOfKm() {
        return costOfKm;
    }

    public void setCostOfKm(float costOfKm) {
        this.costOfKm = costOfKm;
    }
}
