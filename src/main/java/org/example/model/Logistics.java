package org.example.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Logistics {
    private Transport[] vehicles;
    private TransportFactory transportFactory;

    public Transport[] getVehicles() {
        return vehicles;
    }
    public TransportFactory getTransportFactory() {
        return transportFactory;
    }

    public Logistics(Transport... vehicles) {
        this.vehicles = vehicles;
    }

    @Autowired
    public Logistics(TransportFactory transportFactory, Transport... vehicles) {
        this.transportFactory = transportFactory;
        this.vehicles = vehicles;
    }

    public Transport getShipping(City city, int weight, int hours){
        if (vehicles.length == 0){
            this.vehicles = new Transport[1];
            this.vehicles[0] = transportFactory.getTransport(city, weight, hours);
            return vehicles[0];
        }
        else {
            Transport min = null;
        int i = 0;
        while (!isShippingAvailable(city,weight,hours,i) && i < vehicles.length){
            i++;
        }
        if (i < vehicles.length){
            min = vehicles[i];
            while (i < vehicles.length){
                i++;
                if (isShippingAvailable(city,weight,hours,i) && min.getPrice(city) > vehicles[i].getPrice(city)){
                    min = vehicles[i];
                }
            }
        }
        return min;
        }
    }

    private boolean isShippingAvailable (City city, int weight, int hours, int i){
        return i < vehicles.length && vehicles[i].getCapacity() >= weight && vehicles[i].getShippingTime(city) <= hours
                && vehicles[i].getPrice(city) > 0 && !vehicles[i].isRepairing();
    }
}
