package org.example.model;

public class Plane extends Transport {
    public float getPrice (City city) {
        return city.hasAirport ? getCostOfKm() * city.getDistanceKm() : 0;
    }

    public Plane(String name, int capacity, int speed, float costOfKm) {
        super(name, capacity, speed, costOfKm);
    }
}
