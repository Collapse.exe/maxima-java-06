package org.example;

import org.example.model.City;
import org.example.model.Logistics;
import org.example.config.SpringConfig;
import org.example.model.TransportFactory;
import org.example.model.Truck;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        City city = new City("Sada", 500);
        Truck truck = new Truck("truck", 600, 60, 4.44f);
        TransportFactory tr = new TransportFactory();
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        Logistics logistics = context.getBean(Logistics.class);

        System.out.println(logistics.getTransportFactory());
        System.out.println(logistics.getVehicles().length);
        System.out.println(logistics.getShipping(city, 561, 6));
        System.out.println(logistics.getVehicles().length);
        System.out.println(logistics.getVehicles()[0].getSpeed());

        Logistics logistics1 = new Logistics(tr, truck);
        System.out.println(logistics1.getShipping(city, 512, 9));


    }
}
